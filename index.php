

<?php
session_start();
require_once('connection.php');
      if (isset($_GET['controller'])) {
        $controller = $_GET['controller'];
        if (isset($_GET['action'])) {
          $action = $_GET['action'];
        } else {
          $action = 'index';
        }
      } else {
        // var_dump($_SESSION['user']);
        // die;
        if (empty($_SESSION['user'])) { 
          $controller = 'login';
          $action = 'index';
        }
        else {
          
          $controller = 'home';
          $action = 'index';
        }
        
      }
    //require_once('routes.php');
    include_once('controllers/' . $controller . '_controller.php');

    $klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
    $controller = new $klass;
    $controller->$action();
