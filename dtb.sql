create database quanly;
use quanly
drop table department
create table department(
madonvi nvarchar(20) primary key,
tendonvi nvarchar(100),
mota text
);
create table users(
ma nvarchar(20) primary key,
ten nvarchar(100),
quyen nvarchar(20),
taikhoan nvarchar(50),
matkhau nvarchar(50),
madonvi nvarchar(20),
avartar nvarchar(50),
constraint  fk_madonvi foreign key (madonvi) 
references department(madonvi)
);
insert into department values('madv01','abc1','ha noi');
insert into department values('madv02','abc2','ha nam');
insert into department values('madv03','abc3','phu tho');
insert into department values('madv04','abc4','nam dinh');

insert into users values('ma1','anh1','admin','admin1','123','madv01','abc.jpg');
insert into users values('ma2','anh2','admin','admin2','123','madv02','abc.jpg');
insert into users values('ma3','anh3','user','admin3','123','madv03','abc.jpg');
insert into users values('ma4','anh4','user','admin4','123','madv04','abc.jpg');