<?php
class HomeController{
    function __construct()
    {
        // session_start();
        // if (!isset($_SESSION['user']) && !isset($_SESSION['admin'])) {
        //     header('Location: http://localhost/demo_mvc/index.php?controller=login&action=getLogin');
        // }
    }

    public function index() {
        
        // session_unset(); 
        // session_destroy(); 
        // session_start();
        if (!isset($_SESSION['user']) && !isset($_SESSION['admin'])) {
            header('Location: http://localhost/demo_mvc/index.php?controller=login');
        }
        else{
            require("views/index.php");
        }
        
    }
    public function logout(){
        session_unset(); 
        session_destroy(); 
        
        require("views/login/index.php");
    }
}