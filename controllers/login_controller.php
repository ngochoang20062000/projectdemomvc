<?php
require_once('controllers/base_controller.php');
require_once('models/login.php');

class LoginController
{
    private $loginModel;

    function __construct()
    {
        // $this->folder = 'login';
        $this->loginModel = new LoginModel();
    }

    public function index() {
        // Trả dữ liệu về cho View
        // $this->render('index');
        
        require("views/login/index.php");
    }
    
    public function getLogin() {
        session_start();
        // Tiếp nhận dữ liệu mà view gửi lên
        $username = $_POST["username"];
        $password = $_POST["password"];
       
        // xử lý dữ liệu vừa gửi lên và truyền xuống Model

        $result = $this->loginModel->login($username, $password);
        // var_dump($result['quyen']);
        // die;
        if ($result['quyen'] == 'user') {
            $_SESSION['user'] = $result['ma'];
            // session_unset(); 
            // session_destroy(); 
            
        } elseif($result['quyen'] == 'admin') {
            $_SESSION['admin'] = $result['ma'];
            // session_unset(); 
            // session_destroy(); 
           
        }
        
        // render View with data or redirect link
        header("location:index.php?controller=home");
    }
}
