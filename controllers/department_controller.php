<?php
require('models/department.php');
class DepartmentController
{
    
   

        private $departmentModel;

        public function __construct()
        {
            if (!isset($_SESSION['user']) && !isset($_SESSION['admin'])) {
                header('Location: http://localhost/demo_mvc/index.php?controller=login');
            }
            $this->departmentModel = new DepartmentModel;
        }
        public function index(){
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $result = $this->departmentModel->departmentList($page);
            
            require('views/departments/index.php');
        }
        public function create(){
            require('views/departments/add.php');
        }
        public function store(){
   
            $data = [
                'madonvi' => $_POST['madonvi'],
                'tendonvi' => $_POST['tendonvi'],
                'mota' => $_POST['mota'],
            ];
            $this->departmentModel->insert($data);
            $this->index();
        }

        public function edit()
        {
            $madonvi = $_GET['madonvi'];
            $data = $this->departmentModel->findByCode($madonvi);      
            require('views/departments/edit.php');   
        }
        public function destroy(){
            $madonvi = $_GET['madonvi'];
            $data = $this->departmentModel->delete($madonvi);   
            $this->index();      
        }
        public function find($madonvi){

            echo($_GET['madonvi']);
            $data = $this->departmentModel->findByCode($madonvi);
            var_dump($data);
            
        }
        public function update(){
            $madonvi = $_POST['madonvi'];
            $tendonvi = $_POST['tendonvi'];
            $mota = $_POST['mota'];

            $this->departmentModel->updateBy($madonvi,$tendonvi,$mota);
            $this->index();

        }
        public function detail(){
            $madonvi = $_GET['madonvi'];

            $data = $this->departmentModel->detail($madonvi);
            require('views/departments/detail.php');
            
        }

    
    
    
}
?>