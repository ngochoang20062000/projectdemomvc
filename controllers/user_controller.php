<?php
require('models/user.php');
class UserController 
{
    
   

        private $userModel;

        public function __construct()
        {
            if (!isset($_SESSION['user']) && !isset($_SESSION['admin'])) {
                header('Location: http://localhost/demo_mvc/index.php?controller=login');
            }
            $this->userModel = new UserModel;
        }
        public function index(){
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $result = $this->userModel->userList($page);
            // echo "<pre>";
            // var_dump( $result); die;
            
            require('views/users/index.php');
        }
        public function create(){
            require('views/users/add.php');
        }
        public function store(){
   
            $data = [
                'ma' => $_POST['ma'],
                'ten' => $_POST['ten'],
                'quyen' => $_POST['quyen'],
                'taikhoan' => $_POST['taikhoan'],
                'matkhau' => $_POST['matkhau'],
                'madonvi' => $_POST['madonvi'],
                'avartar' => $_POST['avartar'],
            ];
            $this->userModel->insert($data);
            $this->index();
        }
        public function edit()
        {
            $ma = $_GET['ma'];
            $data = $this->userModel->findByCode($ma);      
            require('views/users/edit.php');   
        }
        public function destroy(){
            $ma = $_GET['ma'];
            $data = $this->userModel->delete($ma);   
            $this->index();      
        }
        public function find($ma){

            echo($_GET['ma']);
            $data = $this->userModel->findByCode($ma);
            var_dump($data);
            
        }
        public function update(){
            $ma = $_POST['ma'];
            $ten = $_POST['ten'];
            $quyen = $_POST['quyen'];
            $taikhoan = $_POST['taikhoan'];
            $matkhau = $_POST['matkhau'];
            $madonvi = $_POST['madonvi'];
            $avartar = $_POST['avartar'];

            $this->userModel->updateBy($ma,$ten,$quyen,$taikhoan,$matkhau,$madonvi,$avartar);
            $this->index();

        }
        public function detail(){
            $ma = $_GET['ma'];

            $data = $this->userModel->detail($ma);
            require('views/users/detail.php');
            
        }
    
}
?>