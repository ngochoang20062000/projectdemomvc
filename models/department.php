<?php
class DepartmentModel{

	
	public function __construct()
	{
		// $this->db = new Database();
		// $this->db->connect();
        
	}
	const TABLE = 'department';
	
    public function departmentList($page)
	{
		//so ban ghi hien tren  page
		$list_page = 3;
        $from = ($page - 1) * $list_page;


        $db = DB::getInstance();		
		$sql = "SELECT * FROM department LIMIT $from, $list_page ";
		$result = $db->prepare($sql);
        $result->execute();

        $sql1  = "SELECT COUNT(*) FROM department";
		$result1 = $db->prepare($sql1);
        $result1->execute();
		$sum_user  = $result1->fetch();
		$sum_page = ceil($sum_user[0] / $list_page);
		$list = array();
		$list['sum-page'] = $sum_page;
		while($data = $result->fetch()) {
			$list['data-department'][] = $data;
		}

		return $list;
	}
	public function insert($data){
		$db = DB::getInstance();
		// $sql = "INSERT INTO VALUES department";
		
        
		$columns = implode(',', array_keys($data));
        $values = array_map(function($value) {
            return "'" . $value . "'";
        }, array_values($data));
        $newValues = implode(',', $values);

		$table = self::TABLE;
		
        $sql = "INSERT INTO ${table}(${columns}) VALUES(${newValues})";
		$result = $db->prepare($sql);
        $result->execute();
		
	}

	public function findByCode($madonvi)
	{
		$db = DB::getInstance();		
		$sql = "SELECT * FROM department where madonvi = '$madonvi'";
		$result = $db->prepare($sql);
        $result->execute();
		
		
		$data = $result->fetch();

		return $data;
		
		
	}
	public function delete($madonvi){
		//echo $madonvi;
		$db = DB::getInstance();		
		$sql = "DELETE FROM department where madonvi = '$madonvi'";

		$result = $db->prepare($sql);
        $result->execute();
	}
	public function updateBy($madonvi,$tendonvi,$mota)
	{
		$db = DB::getInstance();		
		$sql = "UPDATE department SET tendonvi = '$tendonvi' ,mota = '$mota' where madonvi = '$madonvi'";
		$result = $db->prepare($sql);
        $result->execute();
	}
	public function detail($madonvi){
		$db = DB::getInstance();		
		$sql = "SELECT * FROM users where madonvi = '$madonvi'";
		$result = $db->prepare($sql);
        $result->execute();

		
		$list = array();
		while($data = $result->fetch()) {
			$list[] = $data;
		}

		return $list;
	}
	
}