<?php
// require('connection.php');
class UserModel{

	
	public function __construct()
	{
		// $this->db = new Database();
		// $this->db->connect();
        
	}

	const TABLE = 'users';

    public function userList($page)
	{
		//so ban ghi hien tren  page
		$list_page = 3;
        $from = ($page - 1) * $list_page;

        $db = DB::getInstance();
		$sql = "SELECT * FROM users LIMIT $from, $list_page ";
		// var_dump($sql); die;
		$result = $db->prepare($sql);
        $result->execute();


        $sql1  = "SELECT COUNT(*) FROM users";
		$result1 = $db->prepare($sql1);
        $result1->execute();
		$sum_user  = $result1->fetch();
		$sum_page = ceil($sum_user[0] / $list_page);
		$list = array();
		$list['sum-page'] = $sum_page;
		while($data = $result->fetch()) {
			$list['data-user'][] = $data;
		}
		
		return $list;
	}
	public function insert($data){
		$db = DB::getInstance();
       
		$columns = implode(',', array_keys($data));
        $values = array_map(function($value) {
            return "'" . $value . "'";
        }, array_values($data));
        $newValues = implode(',', $values);

		$table = self::TABLE;
		
        $sql = "INSERT INTO ${table}(${columns}) VALUES(${newValues})";
		$result = $db->prepare($sql);
        $result->execute();

	}
	public function findByCode($ma)
	{
		$db = DB::getInstance();		
		$sql = "SELECT * FROM users where ma = '$ma'";
		$result = $db->prepare($sql);
        $result->execute();
		
		
		$data = $result->fetch();

		return $data;
		
		
	}
	public function delete($ma){
		
		$db = DB::getInstance();		
		$sql = "DELETE FROM users where ma = '$ma'";

		$result = $db->prepare($sql);
        $result->execute();
	}
	public function updateBy($ma,$ten,$quyen,$taikhoan,$matkhau,$madonvi,$avartar)
	{
		$db = DB::getInstance();		
		$sql = "UPDATE users SET ten = '$ten' ,quyen = '$quyen' ,taikhoan = '$taikhoan' ,matkhau = '$matkhau' ,madonvi = '$madonvi' ,avartar = '$avartar' where ma = '$ma'";
		$result = $db->prepare($sql);
        $result->execute();
	}
	public function detail($ma){
		$db = DB::getInstance();		
		$sql = "SELECT * FROM users where ma = '$ma'";
		$result = $db->prepare($sql);
        $result->execute();

		
		$list = array();
		while($data = $result->fetch()) {
			$list[] = $data;
		}

		return $list;
	}
	public function count(){

		$db = DB::getInstance();
		$sql = "SELECT COUNT(ma) FROM users";
		$result = $db->prepare($sql);

        $result->execute();
		$data=$result->fetch();
		// var_dump($data);
		return $data;
	}
}