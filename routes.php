<?php
$controllers = array(
  'login' => ['index', 'getLogin'],
  'home' => ['index'],
  'department' => ['index'],
  'user'=>['index'],
  'department' => ['create'],
  'department' => ['edit'],
  'department' => ['update'],
  'department' => ['destroy'],
  'user'=>['create'],
  'user'=>['edit'],
  'user'=>['update'],
  'user'=>['destroy'],
);

if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
  $controller = 'login';
  $action = 'index';
}

include_once('controllers/' . $controller . '_controller.php');

$klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
$controller = new $klass;
$controller->$action();