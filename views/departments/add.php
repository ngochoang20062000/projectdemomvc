<!DOCTYPE html>
<html lang="zxx">

<head>
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Rikkeisoft</title>

    
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/bootstrap.min.css" />    
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/metisMenu.css">    
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/style.css" />
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/colors/default.css" id="colorSkinCSS">
</head>
<body class="crm_body_bg">
    



<nav class="sidebar">
    <div class="logo d-flex justify-content-between">
        <a href="index.html"><img src="/demo_mvc/assets/images/img/Logo-Rikkei.png" alt=""></a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="mm-active">
          <a class="has-arrow"  href="#"  aria-expanded="false">
          
          <img src="/demo_mvc/assets/images/img/menu-icon/1.svg" alt="">
            <span>Menu quản trị</span>
          </a>
          <ul>
            <li><a class="active" href="http://localhost/demo_mvc/index.php?controller=department">Department</a></li>
            <li><a class="active" href="http://localhost/demo_mvc/index.php?controller=user">User</a></li>
            
          </ul>

        </li>

        
        
      </ul>
    
</nav>
<section class="main_content dashboard_part">
        
    <div class="container-fluid no-gutters">
        <div class="row">
            <div class="col-lg-12 p-0">
                <div class="header_iner d-flex justify-content-between align-items-center">
                    <div class="sidebar_icon d-lg-none">
                        <i class="ti-menu"></i>
                    </div>
                    <div class="serach_field-area">
                            <div class="search_inner">
                                <form action="#">
                                    <div class="search_field">
                                        <input type="text" placeholder="Search here..." >
                                    </div>
                                    <button type="submit"> <img src="/demo_mvc/assets/images/img/icon/icon_search.svg" alt=""> </button>
                                </form>
                            </div>
                        </div>
                    <div class="header_right d-flex justify-content-between align-items-center">
                        <div class="header_notification_warp d-flex align-items-center">
                            <li>
                                <a href="#"> <img src="/demo_mvc/assets/images/img/icon/bell.svg" alt=""> </a>
                            </li>
                            <li>
                                <a href="#"> <img src="/demo_mvc/assets/images/img/icon/msg.svg" alt=""> </a>
                            </li>
                        </div>
                        <div class="profile_info">
                            <img src="/demo_mvc/assets/images/img/avatar.jpg" alt="#">
                            <div class="profile_info_iner">
                                <p>Welcome Admin!</p>
                                <h5>Trần Ngọc Hoàng</h5>
                                <div class="profile_info_details">
                                    <a href="#">My Profile <i class="ti-user"></i></a>
                                    <a href="#">Settings <i class="ti-settings"></i></a>
                                    <a href="#">Log Out <i class="ti-shift-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_content_iner ">        
    <form method="POST" action="index.php?controller=department&action=store">
        <h1>Thêm mới</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Mã đơn vị</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="madonvi" >
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Tên đơn vị</label>
    <input type="text" class="form-control" id="exampleInputPassword1"  name="tendonvi">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Mô tả</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="mota" >
    
  </div>
  
  <button type="submit" class="btn btn-primary">Thêm</button>
</form>                                
        </div>
    </div>
<div class="footer_part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer_iner text-center">
                    <p>2020 © Influence - Designed by <a href="#"> <i class="ti-heart"></i> </a><a href="#"> Dashboard</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


</body>
</html>