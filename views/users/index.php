
<!DOCTYPE html>
<html lang="zxx">

<head>
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Rikkeisoft</title>

    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/bootstrap.min.css" />    
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/metisMenu.css">    
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/style.css" />
    <link rel="stylesheet" href="/demo_mvc/assets/stylesheets/css/colors/default.css" id="colorSkinCSS">
    <style>
    body {
        font: normal medium/1.4 sans-serif;
        }
    table {
        
    border-collapse: collapse;
    width: 100%;
    
    }
    th, td {
    padding: 0.25rem;
    text-align: center;
    border: 1px solid #ccc;
    
    }
    .pagination {
        
  list-style: none;
  display: inline-block;
  padding: 0;
  margin-top: 10px;
}
.pagination li {
  display: inline;
  text-align: center;
  
}
.pagination a {
  border-radius: 20px ;
  float: left;
  display: block;
  font-size: 20px;
  text-decoration: none;
  padding: 5px 12px;
  color: red;
  margin-left: -1px;
  border: 1px solid black;
  line-height: 1.5;
  background-color: yellow;
}

    </style>
</head>
<body class="crm_body_bg">
    



<nav class="sidebar">
    <div class="logo d-flex justify-content-between">
        <a href="index.html"><img src="/demo_mvc/assets/images/img/Logo-Rikkei.png" alt=""></a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="mm-active">
          <a class="has-arrow"  href="#"  aria-expanded="false">
          
          <img src="/demo_mvc/assets/images/img/menu-icon/1.svg" alt="">
            <span>Menu quản trị</span>
          </a>
          <ul>
            <li><a class="active" href="http://localhost/demo_mvc/index.php?controller=department">Department</a></li>
            <li><a class="active" href="http://localhost/demo_mvc/index.php?controller=user">User</a></li>
            
          </ul>

        </li>

        
        
      </ul>
    
</nav>
<section class="main_content dashboard_part">
        
    <div class="container-fluid no-gutters">
        <div class="row">
            <div class="col-lg-12 p-0">
                <div class="header_iner d-flex justify-content-between align-items-center">
                    <div class="sidebar_icon d-lg-none">
                        <i class="ti-menu"></i>
                    </div>
                    <div class="serach_field-area">
                            <div class="search_inner">
                                <form action="#">
                                    <div class="search_field">
                                        <input type="text" placeholder="Search here..." >
                                    </div>
                                    <button type="submit"> <img src="/demo_mvc/assets/images/img/icon/icon_search.svg" alt=""> </button>
                                </form>
                            </div>
                        </div>
                    <div class="header_right d-flex justify-content-between align-items-center">
                        <div class="header_notification_warp d-flex align-items-center">
                            <li>
                                <a href="#"> <img src="/demo_mvc/assets/images/img/icon/bell.svg" alt=""> </a>
                            </li>
                            <li>
                                <a href="#"> <img src="/demo_mvc/assets/images/img/icon/msg.svg" alt=""> </a>
                            </li>
                        </div>
                        <div class="profile_info">
                            <img src="/demo_mvc/assets/images/img/avatar.jpg" alt="#">
                            <div class="profile_info_iner">
                                <p>Welcome Admin!</p>
                                <h5>Trần Ngọc Hoàng</h5>
                                <div class="profile_info_details">
                                    <a href="#">My Profile <i class="ti-user"></i></a>
                                    <a href="#">Settings <i class="ti-settings"></i></a>
                                    <a href="index.php?controller=home&action=logout">Log Out <i class="ti-shift-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_content_iner ">
    <h1>Danh sách User</h1>
    <?php if (isset($_SESSION['admin']))  {   ?>
        <a href="index.php?controller=user&action=create" style="border-radius: 5px ;padding: 3px 12px;border:1px solid black;background-color:yellow">Create</a> 
    <?php  }   ?>   
    
        
     
    <table>
    <thead>
        <tr>
            <th>STT</th>
            <th>Mã</th>
            <th>Tên</th>
            <th>Quyền</th>
            <th>Tài khoản</th>
            <th>Mật khẩu</th>
            <th>Mã đơn vị</th>
            <th>Avatar</th> 
            <th>Chức năng</th>
          </tr>
        </thead>
        <tbody>
        <?php
            foreach ($result['data-user'] as $key=>$user) {?>
            <tr>
            <td style="padding: 20px 12px"><?php echo($key+1)?></td>
            <td><?=$user['ma']?></td>
            <td><?=$user['ten']?></td>
            <td><?=$user['quyen']?></td>
            <td><?=$user['taikhoan']?></td>
            <td><?=$user['matkhau']?></td>
            <td><?=$user['madonvi']?></td>
            <td>
                <img src="/demo_mvc/assets/images/<?=$user['avartar']?>" class="rounded"   width="50" height="50">
            </td>
            <td>
            <?php if (isset($_SESSION['admin']))  {   ?>
                <a href="index.php?controller=user&action=edit&ma=<?=$user['ma']?>" style="border-radius: 5px ;padding: 3px 12px;border:1px solid black;background-color:yellow;">Edit</a>
                <a onClick="return confirm('Bạn có muốn xóa không?')" href="index.php?controller=user&action=destroy&ma=<?=$user['ma']?>" style="border-radius: 5px ;padding: 3px 12px;border:1px solid black;background-color:lightgreen;">Delete</a>
                <a href="index.php?controller=user&action=detail&ma=<?=$user['ma']?>" style="border-radius: 5px ;padding: 3px 12px;border:1px solid black;background-color:lightpink;">Detail</a>
            <?php  }   ?>   
            
          </td>
            </tr>
            <?php }
          ?>
        
        
        <tbody>
    </table>  
    <ul class="pagination modal-1">
    <li><a href="#" class="">&laquo</a></li>
    <?php for ($i = 1; $i <=$result['sum-page']; $i++) : ?>
            <li class="" style="display: inline;text-align: center;">
              <a href="index.php?controller=user&page=<?=$i?>"><?= $i ?></a>
             </li>
    <?php endfor; ?>
    <li class=""><a>..</a></li>
    <li><a href="#" class="">&raquo</a></li>
    </ul>
    
        </div>
    </div>
<div class="footer_part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer_iner text-center">
                    <p>2020 © Influence - Designed by <a href="#"> <i class="ti-heart"></i> </a><a href="#"> Dashboard</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


</body>
</html>
<!-- <?php
echo($_SESSION['admin']);
echo($_SESSION['user']);
?> -->